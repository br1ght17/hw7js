// 1 

function isPalindrome(str){
    return str===str.split("").reverse().join("")
}

console.log(isPalindrome("reder"))

// 2

function srtLength(str, length){
    return str.length <= +length
}

console.log(srtLength('checked string', 10))
console.log(srtLength('checked string', 20))

// 3

let age = prompt("Напишіть свою дату народження у форматі день.місяць.рік");
let birth = age.split(".");
function fullYears(birth){
    let birthDate = new Date(+birth[2],+birth[1] - 1,+birth[0]);
    let curDate = new Date();
    let ageMillisec = curDate - birthDate
    return Math.floor(ageMillisec / (1000 * 60 * 60 * 24 * 365.25));
}
console.log(fullYears(birth));